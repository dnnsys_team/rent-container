import $ from 'jquery';
import magnificPopup from 'magnific-popup';
import PhotoSlider from './views/PhotoSlider';
import lightGallery from 'lightgallery';
import lgThumbnail from 'lg-thumbnail';
import lgFullscreen from 'lg-fullscreen';

$.fn.extend({
    animateCss: function (animationName) {
        let animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

class Application{
    constructor(){
        document.addEventListener('DOMContentLoaded', () => {
            new PhotoSlider();
            this._galleryThumbnails();
            this._backToTop();
            this._ajaxFormSend();
            this._headerNavigation();
            this._yandexMapInit();
            this._triggerAnimation();
            this._modalActions();
            this._popupInit();
            this._burgerMenu();
            this._videoPlayBtnAnimate();
        })
    }

    _videoPlayBtnAnimate(){
        $('.open-video img').hover(function () {
            $(this).toggleClass('animated pulse');
        });
    }

    _burgerMenu(){
        $('.header__burger').click(function(){
            $(this).toggleClass('open');
            $('.header__nav-menu--mobile').toggleClass('header__nav-menu--open');
        });
    }

    _popupInit(){
        $('.open-video').magnificPopup({
            type: 'iframe'
        });
    }

    _modalActions(){
        $('.btn-call').click('click', () => {
            this._openModal();
        });
        $('#close-modal').click('click', () => {
            this._closeModal();
        });
    }

    _openModal(){
        let $modalWindow = $('#modal-window');
        let $overlay = $('#overlay');
        $overlay.fadeIn();
        $modalWindow.fadeIn('slow');
    }

    _closeModal(){
        let $modalWindow = $('#modal-window');
        let $overlay = $('#overlay');
        $overlay.fadeOut();
        $modalWindow.fadeOut('slow');
    }

    _triggerAnimation(){
        let $massAnimation = $('[data-animate]');
        let $window = $(window);
        $massAnimation.css('visibility', 'hidden');
        //VIEWPORT TRIGGER
        $window.on('scroll', function () {
            $massAnimation.each(function () {
                let $this = $(this);

                if($this.data('isAnimate')) return;

                if ( $window.scrollTop() + ($window.height() * 0.9 ) > $this.offset().top) {
                    let animation = $this.data('animate');
                    let delay = $this.data('delay');
                    $this.css('animation-delay', delay + 's');
                    $this.animateCss(animation);
                    $this.css('visibility', '');
                    $this.data('isAnimate', true)
                }
            });
        }).trigger('scroll');
    }

    _yandexMapInit(){
        ymaps.ready(init);
        function init() {
            //let clickedMarker;
            let myMap = new ymaps.Map('map', {
                    center: [55.76, 37.64],
                    zoom: 10
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                objectManager = new ymaps.ObjectManager({
                    // Чтобы метки начали кластеризоваться, выставляем опцию.
                    clusterize: false,
                    // ObjectManager принимает те же опции, что и кластеризатор.
                    gridSize: 32,
                    clusterDisableClickZoom: true
                });

            // Чтобы задать опции одиночным объектам и кластерам,
            // обратимся к дочерним коллекциям ObjectManager.
            //objectManager.objects.options.set('preset', 'islands#greenDotIcon');
            objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
            myMap.geoObjects.add(objectManager);

            $.ajax({
                url: "data.json"
            }).done(function(data) {
                objectManager.add(data);
            });
            myMap.behaviors.disable('scrollZoom');
        }
    }

    _headerNavigation(){
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();
            let $this = $(this);
            let el = $this.attr('href');
            $('html, body').animate({
                scrollTop: $(el).offset().top
            }, 2000);
        });
    }

    _backToTop() {
        if ($('#back-to-top').length) {
            let scrollTrigger = 100, // px
                backToTop = function () {
                    let scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('#back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
    }

    _galleryThumbnails() {
        $('.photo-slider').lightGallery({
            thumbnail:true,
            selector: '.photo-slider__link'
        });
    }

    _ajaxFormSend(){
        $('form').submit( function(e){
            e.preventDefault();
            let $thisForm = $(this);
            let $url = $thisForm.attr('action');
            let $formMethod = $thisForm.attr('method');
            $('.call-back-form__loader-overlay').fadeIn('slow');
            $.ajax({
                type: 'post',
                url: $url,
                data: $thisForm.serialize()
            }).done(data => {
                console.log(data);
                // if (data.hasOwnProperty('"success"')) {
                    console.log('in success');
                    $('.call-back-form__loader-overlay').fadeOut('slow');
                    $thisForm[0].reset();
                    $thisForm.fadeOut('slow');
                    $thisForm.parent().find('.modal-window__name').fadeOut('fast');
                    $thisForm.parent().find('.call-back-form__success').fadeIn('slow');
                    setTimeout(function () {
                        $thisForm.parent().find('.call-back-form__success').fadeOut('slow');
                        $thisForm.parent().find('.modal-window__name').fadeIn('slow');
                        $thisForm.fadeIn('slow');
                    }, 3000);
                // } else if (data.hasOwnProperty('"error"')) {
                //     $('.call-back-form__loader-overlay').fadeOut('slow');
                //     $thisForm.parent().find('.call-back-form__fail-recaptcha').fadeIn('fast');
                //     setTimeout(function () {
                //         $thisForm.parent().find('.call-back-form__fail-recaptcha').fadeOut('fast');
                //     }, 3000)
                // }
            }).fail(data => {

            });
        });
    }
}

new Application();
