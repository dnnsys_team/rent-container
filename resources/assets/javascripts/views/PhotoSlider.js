import $ from 'jquery';
window.jQuery = $;
window.$ = $;
let owlCarousel = require('owl.carousel');

class PhotoSlider{
    constructor () {
        let fixOwl = function(){
            let $stage = $('.owl-stage'),
                stageW = $stage.width(),
                $el = $('.owl-item'),
                elW = 0;
            $el.each(function() {
                elW += $(this).width()+ +($(this).css("margin-right").slice(0, -2))
            });
            if ( elW > stageW ) {
                $stage.width( elW );
            };
        }
        $('.photo-slider').owlCarousel({
            center: false,
            loop: false,
            stagePadding: 100,
            margin: 50,
            autoWidth: true,
            onInitialized: fixOwl,
            onRefreshed: fixOwl
        });
    }
}
export default PhotoSlider;
